#include "leaf.h"

/*
 * Creates a leaf object to store various values and objects.
 */
Leaf::Leaf(int al, int be, Board *bo, Move *mo, Leaf *p)
{
	alpha = al;
	beta = be;
	leafboard = bo;
	parent_move = mo;
	parent = p;
}
Leaf::~Leaf()
{
}
