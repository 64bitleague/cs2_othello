#include <iostream>
#include "common.h"
#include "board.h"

class Leaf {
    
public:
    int alpha, beta;
    Board *leafboard;
    Move *parent_move;
    Leaf *parent;
    
    Leaf(int al, int be, Board *bo, Move *mo, Leaf *p);
    ~Leaf();
};
