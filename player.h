#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include "leaf.h"
using namespace std;

class Player {

private:
    Board *board;   // Player itself keeps track of the Board state
    Side side, opp_side;    // Own side and opponent's side

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
	Move *heuristic(vector<Move*> move_list);
    void setBoard(Board *b);
    Move *minimax(int ply);
	int AlphaBeta(Leaf node, int level, bool MaxPlayer, Side side, Side opp_side);
	
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

};

#endif
