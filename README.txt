What each group member contributed

Richie:
- wrote alpha-beta pruning function
- added leaf [node] class

Joanne:
- wrote minimax function (some functionality moved to alpha-beta function)
- wrote initial player

Both:
- heuristic calculation function
- debugging
- added several new Board class moves: getValidMoves, getHeuristic
- fixed myriad memory leaks

Improvements

We implemented alpha-beta pruning. Trying it out with minimax at various depths, it seems to work better than just the simple heuristic. We are now able to beat ConstantTimePlayer more frequently than with the simple heuristic. We also tweaked the heuristic calculation to make edge pieces a little more valuable (but not weighted as much as corners).