#include "player.h"
// AI needs a minimum of this many seconds in order to use minimax
#define TIME_BOUND 30
// Heuristic score can never go outside of these values. Modifying the
// constants used to calculate the simple heuristic may require these to change
// as well. Used to initialize variables.
#define MINIMUM -100
#define MAXIMUM 100
// Minimax level (= MINIMAX_LEVEL + 1)
#define MINIMAX_LEVEL 8

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    
    this->side = side;
    this->opp_side = (side == BLACK ? WHITE : BLACK);
    this->board = new Board();
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
     
     // Process the opponent's move first.
    if(opponentsMove != NULL)
    {
        board->doMove(opponentsMove, opp_side);
    }

    Move *m = NULL;
    
    // If we have enough time, do minimax. Otherwise, use simple heuristic.
    if(msLeft >= 1000 * TIME_BOUND || msLeft == -1)
    {
        m = minimax(MINIMAX_LEVEL);
        if(m != NULL)
			board->doMove(m, side);
        return m;
    }
    
    // Find all valid moves and pick the one with the highest heuristic value.
    vector<Move*> move_list = board->getValidMoves(side);
    if(move_list.size() != 0)
    {
        m = heuristic(move_list);
        board->doMove(m, side);
        for(int i = 0; i < (int)move_list.size(); i++)
        {
            if (move_list[i] != m)
            {
                delete move_list[i];
            }
        }
	}
		
    return m;
}

/*
 * Uses simple heuristic calculation to find the optimal best move to make.
 * Returns the move to make.
 */
Move *Player::heuristic(vector<Move*> move_list)
{
	int heuristic;
	int max = MINIMUM, location = 0;
	for(int i = 0; i < (int)move_list.size(); i++)
	{
        heuristic = board->getHeuristic(move_list[i], side, opp_side);
		if (heuristic > max)
		{
			max = heuristic;
			location = i;
		}
        
	}
    
	return move_list[location];
}

void Player::setBoard(Board *b)
{
    this->board = b;
}

/*
 * Creates a minimax tree to find best to n-ply depth. Uses alpha-beta pruning.
 */
Move *Player::minimax(int ply)
{
    Move *m = NULL;
    int score = MINIMUM;
    vector<Move*> move_list = board->getValidMoves(side);
    Leaf parent(0, 0, board, NULL, NULL);
    // Call alpha-beta pruning function on every possible move from current board
    // The Move m will be the move with the highest heuristic from minimax.
    for (int i = 0; i < (int)move_list.size(); i++)
    {
        Board *b = board->copy();
        b->doMove(move_list[i], side);
        Leaf n(MINIMUM, MAXIMUM, b, move_list[i], &parent);
        int s = AlphaBeta(n, ply, false, side, opp_side);
        if (s > score)
        {
            score = s;
            m = move_list[i];
        }
        delete b;
    }
    
    for(int j = 0; j < (int)move_list.size(); j++)
    {
        if (m != move_list[j])
        {
            delete move_list[j];
        }
	}
    return m;
}

/*
 * Alpha beta pruning for minimax trees implementation.
 */
int Player::AlphaBeta(Leaf node, int level, bool MaxPlayer, Side side, Side opp_side)
{
	// If at bottom of tree or if there are no moves left to make, return
	// heuristic value of move
    if(level == 0 || !node.leafboard->hasMoves(MaxPlayer ? side : opp_side))
		return node.parent->leafboard->getHeuristic(node.parent_move, side, opp_side);

    // Current depth of tree is maximizing player's turn
	if(MaxPlayer)
	{
		vector<Move*> move_list = node.leafboard->getValidMoves(side);
		for(int i = 0; i < (int)move_list.size(); i++)
		{
			Board *childboard = node.leafboard->copy();
			childboard->doMove(move_list[i], side);
			Leaf child(node.alpha, node.beta, childboard, move_list[i], &node);
			int child_value = AlphaBeta(child, level - 1, false, side, opp_side);
			node.alpha = (node.alpha > child_value) ? node.alpha : child_value;
			delete childboard;
			if(node.beta <= node.alpha)
				break;
		}
		for(int n = 0; n < (int)move_list.size(); n++)
			delete move_list[n];
		return node.alpha;
	}
    // Current depth of tree is NOT maximizing player's turn
	else
	{
		vector<Move*> move_list = node.leafboard->getValidMoves(opp_side);
		for(int j = 0; j < (int)move_list.size(); j++)
		{
			Board *childboard = node.leafboard->copy();
			childboard->doMove(move_list[j], opp_side);
			Leaf child(node.alpha, node.beta, childboard, move_list[j], &node);
			int child_value = AlphaBeta(child, level - 1, true, side, opp_side);
			node.beta = (node.beta < child_value) ? node.beta : child_value;
			delete childboard;
			if(node.beta <= node.alpha)
				break;
		}
		for(int n = 0; n < (int)move_list.size(); n++)
			delete move_list[n];
		return node.beta;
	}
}
